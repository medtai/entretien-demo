import { Page } from "@playwright/test"
import selector from "../../src/selectors/selectors.json"

export default class Function {
    public page: Page;
    constructor(page: Page) {
        this.page = page;
    }


    public async clickOnFirstStart(): Promise<void> {
        //await this.page.pause();
        await this.page.locator(selector.firstStartButton).waitFor({ state: 'visible' });
        await this.page.locator(selector.firstStartButton).click();
    
    }
    public async clickOnSecondStart(): Promise<void> {
        await this.page.locator(selector.secondStartButton).waitFor({ state: 'visible' });
        await this.page.locator(selector.secondStartButton).click();
    }
    public async fillFirsName(firstName: string): Promise<void> {
        await this.page.locator(selector.firstName).waitFor({ state: 'visible' });
        await this.page.locator(selector.firstName).fill(firstName);
    }
    public async fillLastName(lastName: string): Promise<void> {
        await this.page.locator(selector.lastName).fill(lastName);
    }
    public async fillEmailAdress(email: string): Promise<void> {
        await this.page.locator(selector.emailField).fill(email);
    }
    public async fillCampanyName(cmpName: string): Promise<void> {
        await this.page.locator(selector.campanyName).fill(cmpName);
    }
    public async fillCampanyWebSite(webSite: string): Promise<void> {
        await this.page.locator(selector.campanyWebSite).fill(webSite);
       
    }
    public async clickOnIgreeCheckBox(): Promise<void> {
        await this.page.locator(selector.iAgreeCheckBox).click();
    }
    public async clickOnSubmit(): Promise<void> {
        await this.page.locator(selector.submitButton).click();
    }
}