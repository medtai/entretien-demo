import { test, expect, Page } from '@playwright/test';
import Methods from "../src/pages/Function";

test('', async ({ page }) => {

    let func : Methods = new Methods(page);
    await page.goto("https://www.swan.io/")
    await func.clickOnFirstStart();
    await func.clickOnSecondStart();
    await func.fillFirsName("toto");
    await func.fillLastName("tata");
    await func.fillEmailAdress("toto@gmail.com");
    await func.fillCampanyName("swan");
    await func.fillCampanyWebSite("www.swan.io");
    await func.clickOnIgreeCheckBox();
    await func.clickOnSubmit();


});
